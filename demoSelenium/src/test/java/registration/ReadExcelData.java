package registration;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelData {

	public String[][] read_Sheet() throws InvalidFormatException, IOException{
		File myFile=new File(".\\TestData\\Infos.xlsx");
		XSSFWorkbook wb=new XSSFWorkbook(myFile);
		XSSFSheet mysheet=wb.getSheet("Sheet1");
		
		//get numbre rows and columns in my excel
		int numberOfRows=mysheet.getPhysicalNumberOfRows();
		int numberOfColums=mysheet.getRow(0).getLastCellNum();
		
		//numberOfRows-1 to eliminate the header of excel
		String[][] myArray=new String[numberOfRows-1][numberOfColums];
		
				for( int i=1 ; i<numberOfRows; i++) {
					for (int a=0;a<numberOfColums;a++) {
						XSSFRow row=mysheet.getRow(i);
						myArray [i-1][a]=row.getCell(a).getStringCellValue();					}
				}
		return myArray;
	}
}

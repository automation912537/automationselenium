package registration;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.Set;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestRegistration {

	/*
	 * TEST REGESTRATION IN A WEBSITE STEPS /
	 * 1.TEST THE OPENING OF THE BROWSER
	 * 2.TEST REGISTRATION*/
	
	WebDriver amDriver;

	// this tests takes data from the method testData which reads from excel
	//pass the data names ( name/email... ) in the test method params
	@Test(dataProvider = "test_Data")
	public void test_registration_(String firstName ,String lastName ,String phone ,String mail, String password , String postcode, String confirm ,String adress1 , String pwd ,String state ,String city) {
	    WebDriverWait wait = new WebDriverWait(amDriver, 30);
		JavascriptExecutor js = (JavascriptExecutor) amDriver;

		WebElement signUp =amDriver.findElement(By.xpath("/html/body/header/nav/div/button"));
		signUp.click();
		
		// FIND BROWSER signUp BUTTON BY HREF NAME
		WebElement firstNameElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"mynavbar\"]/div/a[2]")));
		js.executeScript("arguments[0].scrollIntoView(true);", firstNameElement);
		firstNameElement.click();
		
		// Get new page for form
	    amDriver.get("https://phptravels.org/register.php");

		//ADD NECESSERY DATA TO SIGNUP IN NEWLY OPENED WINDOW SO U SHOULD OPEN THE NEW WINDOW
		amDriver.findElement(By.name("firstname")).sendKeys(firstName);
		amDriver.findElement(By.name("lastname")).sendKeys(lastName);
		amDriver.findElement(By.name("email")).sendKeys(mail);
		amDriver.findElement(By.name("phonenumber")).sendKeys(phone);
		amDriver.findElement(By.name("password")).sendKeys(pwd);
		amDriver.findElement(By.name("password2")).sendKeys(confirm);
		amDriver.findElement(By.name("address1")).sendKeys(adress1);
		//amDriver.findElement(By.name("address1")).sendKeys("nabeul");
		amDriver.findElement(By.name("postcode")).sendKeys(postcode);
		amDriver.findElement(By.name("state")).sendKeys(state);
		amDriver.findElement(By.name("city")).sendKeys(city);
		// Click the "I am not a robot" checkbox : captchaCheckbox is the className
        amDriver.findElement(By.className("g-recaptcha")).click();
         
        //REGISTER btn
        //click btn using inspect > copy > XPath
        WebElement submitBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"frmCheckout\"]/p/input")));
        js.executeScript("scroll(0,250)"); 
        submitBtn.click();

        //Vérifie si un message d'erreur apparaît pour l'inscription de ce mail déja registré
        WebElement errorDiv = amDriver.findElement(By.cssSelector(".alert.alert-danger"));
        String errorMessage = errorDiv.getText();
   //     assertEquals(errorMessage , "A user already exists with that email address Please complete the captcha and try again.");  
        assertTrue(errorMessage.contains("A user already exists with that email address"));
        
        //AFTER VERIFIYING REGISTRATION ,LOGIN
        amDriver.get(("https://phptravels.org/login.php"));
        
		WebElement email = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
        js.executeScript("arguments[0].scrollIntoView(true);", email);
        email.sendKeys("yossr.bouzayenne@gmail.com");
        
		amDriver.findElement(By.name("password")).sendKeys("SELENIUM123");
		
        WebElement recaptcha =amDriver.findElement(By.className("g-recaptcha"));
        js.executeScript("arguments[0].scrollIntoView(true);", recaptcha);
        recaptcha.click();
        
		WebElement signUpAfterRegister =amDriver.findElement(By.xpath("//*[@id=\"login\"]"));
        js.executeScript("arguments[0].scrollIntoView(true);", signUpAfterRegister);
        signUpAfterRegister.click();
        
        //assert that the box containing user details exists
        amDriver.get("https://phptravels.org/clientarea.php");
        //String userName=amDriver.findElement(By.xpath("//*[@id=\"main-body\"]/div/div[1]/div[1]/div[1]/div/div[2]/div/strong")).getText();
        //assertEquals(userName,"yosr bouzayene ");
		
        WebElement userInfo = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"main-body\"]/div/div[1]/div[1]/div[1]/div")));
        assertNotNull(userInfo);
        
        //try to scroll and find logOut > in order to logIn after this , with a new user from excel
        js.executeScript("scroll(0,250)"); 
        WebElement LogOutBtn =amDriver.findElement(By.xpath("//*[@id=\"Secondary_Sidebar-Client_Shortcuts-Logout\"]"));
        LogOutBtn.click();
        
        
	}
	
	@BeforeClass
	public void open_browser_() {
		
		// 1.modify system properties which are key-value pairs that provide configuration information to JVM and other parts of your Java application
		System.setProperty("webdriver.chrome.driver",".\\drivers\\chromedriver.exe");
		
		// 2. instanciate any type of driver (ChromeDriver is a class that implements the interface WebDriver)
		 amDriver=new ChromeDriver();
		
		// 3.open the desired website using appropriate driver
		amDriver.get("https://phptravels.com/");
	}
	
	@AfterClass
	public void close_browser_() {
		
		amDriver.close();
	}

	//@DataProvider indiquer une méthode qui fournit des données de test à une méthode de test.
	//from my data excel for exemple
	@DataProvider
	public Object[][] test_Data() throws InvalidFormatException, IOException{
		
		ReadExcelData readExcel=new ReadExcelData();
		return readExcel.read_Sheet() ;
				
	}
	
	
}
